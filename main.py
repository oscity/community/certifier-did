#!/usr/bin/env python

import asyncio
import json
import os
import shutil
import threading
import uuid
from os import listdir
from os.path import isfile, join
from unittest import mock
from urllib.parse import urljoin

from dotenv import load_dotenv
from google.cloud import firestore, storage

import requests
import certifier
import configure_certs
from configure_certs import ROSTERS_DIR

load_dotenv()

DATABASE_COLLECTION = os.getenv("DATABASE_COLLECTION")
DOMAIN_URL = os.getenv("DOMAIN_URL")
STORAGE_INPUT_DIR = os.getenv("STORAGE_INPUT_DIR")
STORAGE_OUTPUT_DIR = os.getenv("STORAGE_OUTPUT_DIR")
UNIQUE_KEY = os.getenv("UNIQUE_KEY")
DIR_CLONE_REPOS = "/tmp/blockcerts"
DATA_DIR = os.getenv("DATA_DIR")
CONF_TOOLS_INI = os.getenv("CONF_TOOLS_INI")
CMP_ENDPOINTS_URL = os.getenv("CMP_ENDPOINTS_URL")
CMP_DOMAIN_URL = os.getenv("CMP_DOMAIN_URL")
SEND_EMAIL = os.getenv("SEND_EMAIL")
EMAIL_TEMPLATE_ID = os.getenv("EMAIL_TEMPLATE_ID")

storage_client = storage.Client()
firestore_client = firestore.Client()

# This blockcerts configuration will run only at instance cold-start (function deploy)
configure_certs.configure_blockcerts()

# [START functions_blockchain_certificates_gcs]


def blockchain_certificates_gcs(data, context):
    """
    Background Cloud Function to be triggered by Cloud Storage.
    This generic function logs relevant data when a file is changed.
    Certifies each document in the roster and the result is uploaded
    to a new folder, and Firebase document matching the certificate
    is updated to have the info about the certificate

    Args:
        data (dict): The Cloud Functions event payload.
        context (google.cloud.functions.Context): Metadata of triggering event.
    Returns:
        None; the output is written to Stackdriver Logging
    """
    #  Process file only if it came from input dir
    if data['name'].startswith(STORAGE_INPUT_DIR):
        print(f'Event ID: {context.event_id}')
        print(f'Bucket: {data["bucket"]}')
        print(f'File: {data["name"]}')
        print(f'Created: {data["timeCreated"]}')

        file_name = data['name']
        bucket_name = data['bucket']
        # Download file to blob
        blob = storage_client.bucket(bucket_name).get_blob(file_name)

        if blob is not None:
            blockchain_certificates_path = __process_uploaded_file(blob)
            __process_result_files(blockchain_certificates_path, bucket_name)
            shutil.rmtree(blockchain_certificates_path)
            print('--- Process finished ---')
            return
        else:
            print('404: File not found')
            raise FileNotFoundError('404. File not found in Storage')
    else:
        return


# [END functions_blockchain_certificates_gcs]


def __process_uploaded_file(current_blob):
    """
    Download uploaded file to rosters path to be certified
    Args:
        current_blob: File uploaded to Cloud Storage

    Returns:

    """
    # Download file from bucket to tmp file
    file_name = current_blob.name.replace(STORAGE_INPUT_DIR, '')
    os.makedirs(ROSTERS_DIR, exist_ok=True)
    local_tmp_file = join(ROSTERS_DIR, f"{uuid.uuid4()}.csv")
    current_blob.download_to_filename(local_tmp_file)

    # Clean roster file
    # Remove Windows weird characters (Eg. b'\xef\xbb\xbf' at the beginning of file)
    # https://www.howtosolutions.net/2019/04/python-fixing-unexpected-utf-8-bom-error-when-loading-json-data/
    roster_local_file = join(ROSTERS_DIR, 'roster.csv')
    with open(local_tmp_file, 'rb') as source_file:
        with open(roster_local_file, 'w+b') as dest_file:
            contents = source_file.read()
            dest_file.write(contents.decode('utf-8-sig').encode('utf-8'))

    print(f'File {file_name} was downloaded to {roster_local_file}.')

    # certify data
    blockchain_certificates_path = certifier.certify_data(DATA_DIR)
    return blockchain_certificates_path


# [END functions_process_uploaded_file]


def __process_result_files(json_dir, bucket_name):
    """
    Process individually each certificate to upload result to Cloud Storage
    Updates Firebase document that matches each certificate
    Args:
        json_dir:
        bucket_name:

    Returns:

    """
    threads = list()
    blockchain_certificates = [f for f in listdir(
        json_dir) if isfile(join(json_dir, f)) and os.path.splitext(join(json_dir, f))[1].lower() == '.json']
    print('Files to be read: ', blockchain_certificates)

    for file in blockchain_certificates:
        asyncio.run(__upload_and_update_by_json_match(
            json_dir, file, bucket_name))

    return


async def __upload_and_update_by_json_match(json_dir, file, bucket_name):
    """
    Upload blockchain certificate JSON result to Cloud Storage
    Map data to update Firebase document
    Args:
        json_dir:
        file:
        bucket_name:

    Returns:

    """
    print("Uploading file to output storage")
    await __upload_result_to_bucket(bucket_name, join(
        STORAGE_OUTPUT_DIR, file), join(json_dir, file))
    print(f"Reading file: {file}")
    with open(os.path.join(json_dir, file)) as json_file:
        json_data = json.load(json_file)
        json_data['cert_id'] = json_data['id'].replace('urn:uuid:', '')
        json_data['cert_url'] = f"{DOMAIN_URL}{json_data['cert_id']}"
        print(f"Id: {json_data['cert_id']}")
        print(f"Url: {json_data['cert_url']}")
        print("")
        await __update_document(json_data)
        print('SEND_EMAIL', SEND_EMAIL)
        if SEND_EMAIL and 'email' in json_data:
            print('__send_email_notification')
            await __send_email_notification(json_data)

    return

async def __send_email_notification(json_data):
    """
    Send email notification
    """
    if "cert_url" in json_data:
        button_link = json_data["cert_url"]
    else:
        button_link = urljoin(CMP_DOMAIN_URL, 'citizen/certificates')
    print('CMP_ENDPOINTS_URL', CMP_ENDPOINTS_URL)
    print('link', button_link)
    if CMP_ENDPOINTS_URL:
        print('sendEmail')
        url = urljoin(CMP_ENDPOINTS_URL, 'sendEmail')
        email_content = {
            "personalizations": [
            {
              "to": [
                {
                  "email": json_data["email"],
                },
              ],
              "dynamic_template_data": {
                "name": json_data["firstName"] if "firstName" in json_data else json_data["email"],
                "link": button_link,
                "user": json_data["email"],
                "title": "Certificado digital",
                "subject": "Certificado digital",
                "content": "Tu certificado está listo",
                "button": "Ver Certificado",
              },
              "subject": "Certificado digital",
            },
          ],
        }
        print('EMAIL_TEMPLATE_ID', EMAIL_TEMPLATE_ID)
        if EMAIL_TEMPLATE_ID:
            email_content['template_id'] = EMAIL_TEMPLATE_ID
        print('email_content', email_content)
    else:
        print('send-email')
        url = urljoin(CMP_DOMAIN_URL, '_ah/api/oscity/v1/email/send-mail')
        email_content = {
            "email": json_data["email"],
            "name": json_data['recipientProfile']['name'],
            "link": button_link,
            "content": "Tu certificado está listo",
            "title": "Certificado digital"
        }
    try:
        response = requests.post(url, json=email_content)
        print('response', response)
        response.raise_for_status()
    except requests.exceptions.HTTPError as error:
        raise error
    except requests.exceptions.RequestException as error:
        raise error
    finally:
        return response


async def __upload_result_to_bucket(upload_bucket_name, file_name, temp_local_filename):
    """
    Upload result to a Cloud Storage bucket, in other folder to avoid re-triggering the function.
    Args:
        upload_bucket_name: Bucket name to upload files
        file_name: New file name (folder already pasted at the beginning)
        temp_local_filename: Local file in function env

    Returns: gs:// url reference of uploaded file

    """
    upload_bucket = storage_client.bucket(upload_bucket_name)
    new_blob = upload_bucket.blob(file_name)
    new_blob.upload_from_filename(temp_local_filename)
    print(f'Result uploaded to: gs://{upload_bucket_name}/{file_name}')
    return f"gs://{upload_bucket_name}/{file_name}"


async def __update_document(json_data):
    """
    Update Firebase document based in json_data
    Args:
        json_data: Data to update Firebase document

    Returns:

    """
    #  Set query for unique match
    query = __set_query_unique_match(json_data)

    # Get documents that match
    docs = firestore_client.collection(
        DATABASE_COLLECTION).where(*query).stream()
    # Display document documents
    for doc in docs:
        print(u'{} => {}'.format(doc.id, doc.to_dict()))
        # Get firebase document reference
        document = firestore_client.collection(
            DATABASE_COLLECTION).document(doc.id)
        # Document properties to update
        doc_properties_updated = {
            "link": json_data['cert_url'],
            "is_certified": True,
            "estado_origen": "Certificado"
        }
        print(doc_properties_updated)
        # Update matching document with info from json 
        result = document.update(doc_properties_updated)
        print(result)
    return


def __set_query_unique_match(json_data):
    """
    Creates Firebase query. UNIQUE_KEY should be a unique id property
    Args:
        json_data:

    Returns:

    """
    return UNIQUE_KEY, "==", json_data[UNIQUE_KEY]


if __name__ == '__main__':

    filename = f"TEST_DIR/{str(uuid.uuid4())}"
    data = {
        "bucket": "govtech-certificates.appspot.com",
        "name": "certificados_govtech/ETH_GOVTECH_2020_12_16_12_41_42.csv",
        "metageneration": "some-metageneration",
        "timeCreated": "0",
        "updated": "0"
    }
    context = mock.MagicMock()
    context.event_id = 'some-id'
    context.event_type = 'gcs-event'

    blockchain_certificates_gcs(data, context)
