#!/usr/bin/env python

import os
import shutil
from os.path import join
from string import Template

from cert_tools.create_v2_certificate_template import main as create_certificate_template
from dotenv import load_dotenv

load_dotenv()

DIR_CLONE_REPOS = "/tmp/blockcerts"
DATA_DIR = os.getenv("DATA_DIR")
CONF_TOOLS_INI = os.getenv("CONF_TOOLS_INI")
IMAGE_LOGO = os.getenv("IMAGE_LOGO")
PK_ISSUER = os.getenv("PK_ISSUER")
ISSUING_ADDRESS = os.getenv("ISSUING_ADDRESS")
ISSUER_CHAIN = os.getenv("ISSUER_CHAIN")
ISSUER_API_TOKEN = os.getenv("ISSUER_API_TOKEN", "")
ISSUER_GAS_PRICE = os.getenv("ISSUER_GAS_PRICE", "")

CERT_TOOLS = join(DIR_CLONE_REPOS, 'cert-tools')
CERT_CORE = join(DIR_CLONE_REPOS, 'cert-core')
CERT_ISSUER = join(DIR_CLONE_REPOS, 'cert-issuer2')

ROSTERS_DIR = join(CERT_TOOLS, DATA_DIR, 'rosters')
IMAGES_DIR = join(CERT_TOOLS, DATA_DIR, 'images')
CWD = os.getcwd()


def configure_blockcerts():
    print("Configure blockcerts directories... ")
    __create_directories()
    __configuration_cert_tools()
    __configuration_cert_issuer()
    __create_certificate_template()
    return 'Success'


def __create_directories():
    shutil.rmtree(DIR_CLONE_REPOS, ignore_errors=True)
    os.makedirs(DIR_CLONE_REPOS, exist_ok=True)
    os.makedirs(join(DIR_CLONE_REPOS, "cert-tools"), exist_ok=True)
    os.makedirs(join(DIR_CLONE_REPOS, "cert-issuer2"), exist_ok=True)


def __configuration_cert_tools():
    """
    Configure cert_tools module
    """
    print(CERT_TOOLS)
    os.chdir(CERT_TOOLS)

    # Create needed folders
    print('Creating needed directories...')
    os.makedirs(ROSTERS_DIR, exist_ok=True)
    os.makedirs(join(CERT_TOOLS, DATA_DIR,
                     'unsigned_certificates'), exist_ok=True)
    os.makedirs(join(CERT_TOOLS, DATA_DIR,
                     'certificate_templates'), exist_ok=True)
    os.makedirs(IMAGES_DIR, exist_ok=True)

    # Copy conf_tools.ini file to default PATH and name
    print("Copy conf_tools.ini file to default PATH and name")
    shutil.copy(join(CWD, CONF_TOOLS_INI), join(CERT_TOOLS, 'conf.ini'))
    print(os.path.exists(join(CERT_TOOLS, 'conf.ini')))
    # Copy image logo to path (full replace)
    shutil.rmtree(IMAGES_DIR)
    shutil.copytree(join(CWD, 'images'), IMAGES_DIR)


def __configuration_cert_issuer():
    """
    Configure cert_issuer for certificate json data
    """
    os.chdir(DIR_CLONE_REPOS)
    print(CERT_ISSUER)

    # Generate conf_issuer.ini file in default path
    __generate_conf_issuer()
    # Copy private key issuer
    shutil.copy(join(CWD, PK_ISSUER), join(CERT_ISSUER, PK_ISSUER))
    print(f"Private key {PK_ISSUER} copied to {CERT_ISSUER}")


def __generate_conf_issuer():
    print('Generating conf_issuer.ini ...')
    conf_issuer_template = Template("issuing_address=$issuing_address\n"
                                    "chain=$issuer_chain\n"
                                    "usb_name=$dir_clone_repos/cert-issuer2/\n"
                                    "key_file=$pk_issuer\n"
                                    "unsigned_certificates_dir=$dir_clone_repos/cert-issuer2/data/unsigned_certificates\n"
                                    "blockchain_certificates_dir=$dir_clone_repos/cert-issuer2/data/blockchain_certificates\n"
                                    "work_dir=$dir_clone_repos/cert-issuer2/work\n")
    conf_issuer = conf_issuer_template.substitute(issuing_address=ISSUING_ADDRESS, issuer_chain=ISSUER_CHAIN,
                                                  dir_clone_repos=DIR_CLONE_REPOS, pk_issuer=PK_ISSUER)
    if ISSUER_API_TOKEN:
        conf_issuer += f"api_token={ISSUER_API_TOKEN}\n"
    if ISSUER_GAS_PRICE:
        conf_issuer += f"gas_price={ISSUER_GAS_PRICE}\n"
    conf_issuer += "no_safe_mode\n"

    # conf_issuer.ini
    with open(join(CERT_ISSUER, 'conf.ini'), 'w') as conf_issuer_file:
        conf_issuer_file.write(conf_issuer)
        print(f"conf_issuer.ini was saved in: {join(CERT_ISSUER, 'conf.ini')}")


def __create_certificate_template():
    os.chdir(CERT_TOOLS)
    print("Creating certificate template...")
    try:
        create_certificate_template()
    except Exception:
        print('Some error has occurred creating the template')
        raise
    print("Template was created in cert_tools")


if __name__ == '__main__':
    configure_blockcerts()
