import os
import shutil

from dotenv import load_dotenv

import configure_certs

load_dotenv()

DIR_CLONE_REPOS = "/tmp/blockcerts"


def test_configure_blockcerts():
    # subprocess_mock.run = MagicMock(return_value=0)
    # os_mock.system = MagicMock(return_value=0)
    shutil.rmtree(DIR_CLONE_REPOS, ignore_errors=True)
    res = configure_certs.configure_blockcerts()

    assert res == 'Success'
