#!/bin/python

# It will run a full deployment using gcloud configuration for an already defined project

# Be sure that in your configurations pk_issuer flie is included (not stored in GIT for
# security reasons) and your serviceAccountKey.json was creating using the configuration
# found in gcloud.config file and account has Editor level access (for Storage and Firebase permissions)

# You can deploy an already defined project using the name of the folder inside config/ directory
# python3 run_deploy_gcloud.py <DIRNAME>s

from run_configure_gcloud import *
from run_clean_workspace import *


def deploy_gcloud(project):
    configure_gcloud(project)

    config = configparser.ConfigParser()
    config.read('gcloud.config')
    GCF_NAME = config['DEFAULT']['GCF_NAME']
    PROJECT_ID = config['DEFAULT']['PROJECT_ID']

    subprocess.run(
        f"gcloud functions deploy {GCF_NAME} --runtime python37 --trigger-resource {PROJECT_ID}.appspot.com --trigger-event google.storage.object.finalize --env-vars-file .env.yaml", shell=True, check=True)

    clean_workspace()


if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise ReferenceError("Missing argument <project>")
    project = sys.argv[1]
    deploy_gcloud(project)
