#!/usr/bin/env python
# python certify.py arg1 arg2 arg3
"""
Use block certs modified versions of cert_tools and cert_issuer to certify data
Requires conf_tools.ini file, final client directory name,
"""
import os
import shutil
import sys
from os import listdir
from os.path import isfile, join
from pathlib import Path

from cert_issuer.__main__ import cert_issuer_main as issue_certificates
from cert_tools.instantiate_v2_certificate_batch import main as instantiate_certificate_batch
from dotenv import load_dotenv

load_dotenv()

DIR_CLONE_REPOS = "/tmp/blockcerts"


def remove_files(path_to_remove):
    for p in Path(path_to_remove).glob('*.json'):
        p.unlink()


def move_files(path_from_move, path_to_move):
    filename: str
    for filename in Path(path_from_move).glob('*.json'):
        new_path = os.path.join(path_to_move, os.path.basename(filename))
        shutil.move(filename, new_path)


def certify_data(dir_name):
    print('Creating certificates...')
    """
    Cert tools
    Instantiate certificates batch
    """
    cert_tools = join(DIR_CLONE_REPOS, 'cert-tools')
    cert_issuer = join(DIR_CLONE_REPOS, 'cert-issuer2')
    os.chdir(cert_tools)
    # Create instance unsigned certificates directory
    instance_unsigned_certificates = join(
        cert_tools, dir_name, 'unsigned_certificates')
    os.makedirs(instance_unsigned_certificates, exist_ok=True)

    # Generate unsigned certificates
    __instantiate_certificates_batch(cert_tools)
    print(f"{instance_unsigned_certificates}")
    unsigned_certificates = [f for f in listdir(instance_unsigned_certificates) if
                             isfile(join(instance_unsigned_certificates, f))]
    print(unsigned_certificates)

    """
    Cert issuer
    Issue certificates
    """
    data_unsigned_certificates = join(
        cert_issuer, 'data', 'unsigned_certificates')
    data_blockchain_certificates = join(
        cert_issuer, 'data', 'blockchain_certificates')
    os.makedirs(data_unsigned_certificates, exist_ok=True)
    os.makedirs(data_blockchain_certificates, exist_ok=True)

    # Move generated unsigned certificates
    move_files(instance_unsigned_certificates, data_unsigned_certificates)
    print(f"Unsigned certificates moved to cert_issuer: ")
    print(data_unsigned_certificates)

    print("Certificate unsigned files in a blockchain...")
    __issue_certificates(cert_issuer)
    # Display list of files found in cert_issuer/data/blockchain_certificates
    blockchain_certificates = [f for f in listdir(data_blockchain_certificates) if
                               isfile(join(data_blockchain_certificates, f))]
    print("Blockchain certificates:")
    print(blockchain_certificates)
    if (len(blockchain_certificates) > 0):
        shutil.rmtree(data_unsigned_certificates)

    print("Certify process completed")
    return data_blockchain_certificates


def __instantiate_certificates_batch(cert_tools_path):
    """
    Instantiate certificates with the default configuration
    conf.ini file must be placed in cert_tools_path
    Args:
        cert_tools_path:

    Returns:

    """
    os.chdir(cert_tools_path)
    try:
        instantiate_certificate_batch()
    except Exception as e:
        print("Unexpected error on instantiate_certificate_batch")
        print(e)
        raise


def __issue_certificates(cert_issuer_path: str):
    """
    Creates certificates with the default configuration
    conf.ini file must be placed in cert_issuer_path
    Args:
        cert_issuer_path:

    Returns:

    """
    print("Creating certificates...")
    os.chdir(cert_issuer_path)
    try:
        issue_certificates()
    except Exception as e:
        print("Some error occurred while issuing certificates:")
        print(e)
        raise


if __name__ == '__main__':
    try:
        conf = sys.argv[2]
    except IndexError:
        raise SystemExit(
            f"Missing argument: {sys.argv[0]} <dir_name>")

    certify_data(sys.argv[1])
