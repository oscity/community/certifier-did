#!/bin/python

# You can configure an already existent project (found in config/ folder calling this script)
# using the dirname of the folder as the argument
# bash run_configure_gcloud.sh <DIRNAME>

import configparser
import os
import shutil
import subprocess
import sys

import yaml
from dotenv import load_dotenv

from run_clean_workspace import replace_string

load_dotenv()

if 'PK_ISSUER_BFA' not in os.environ:
    load_dotenv(dotenv_path=".env.local")


def configure_gcloud(selected_project: str):
    print('=' * 100)

    project_path = f'./config/{selected_project}/'
    if os.path.exists(project_path):
        print(f'Configuring project: {selected_project}')
    else:
        raise FileExistsError(f'{project_path} does not exist.')

    copy_cmd = f'cp -r {project_path}. ./'
    subprocess.run(copy_cmd, shell=True, check=True)

    print('=' * 100)

    if os.path.exists('gcloud.config'):
        print('gcloud configuration:')
    else:
        raise FileExistsError('gcloud.config does not exist.')

    # Read gcloud configuration file
    gcloud_config = configparser.ConfigParser()
    gcloud_config.read('gcloud.config')

    project_id = gcloud_config['DEFAULT']['PROJECT_ID']
    gcf_name = gcloud_config['DEFAULT']['GCF_NAME']

    print('.' * 100)
    print(f"Google Cloud Project: {project_id}")
    print(f"Google Cloud Function Name: {gcf_name}")

    print('=' * 100)

    print('Checking files exist:')

    # Test needed files exist
    # Variables file
    if os.path.exists('.env.yaml'):
        print(".env.yaml \t\t\t\t\t\t [OK]")
    else:
        raise FileExistsError(".env.yaml does not exist.")

    subprocess.run(
        "sed -e 's/:[^:\/\/]/=/g;s/$//g;s/ *=/=/g' .env.yaml > .env", shell=True, check=True)

    with open('.env.yaml', 'r') as yaml_file:
        env_variables = yaml.safe_load(yaml_file)

    # Service Account Key
    account_key = gcloud_config['DEFAULT']['ACCOUNT_KEY']
    service_account_key = os.getenv(account_key)
    shutil.copy(service_account_key, 'serviceAccountKey.json')

    if os.path.exists("serviceAccountKey.json"):
        print("serviceAccountKey.json \t\t\t\t\t [OK]")
    else:
        raise FileExistsError("serviceAccountKey.json does not exist.")

    # Private Key Issuer
    pk_issuer_file_variable = gcloud_config['DEFAULT']['PK_ISSUER_FILE']
    pk_issuer_local_filename = os.getenv(pk_issuer_file_variable)
    pk_issuer_final_filename = env_variables['PK_ISSUER']
    shutil.copy(pk_issuer_local_filename, pk_issuer_final_filename)

    if os.path.exists(pk_issuer_final_filename):
        print(f"{pk_issuer_final_filename} \t\t\t\t\t [OK]")
    else:
        raise FileExistsError(f"{pk_issuer_final_filename} does not exist.")

    # Conf tools file
    conf_tools_ini = env_variables['CONF_TOOLS_INI']
    if os.path.exists(conf_tools_ini):
        print(f"{conf_tools_ini} \t\t\t\t\t [OK]")
    else:
        raise FileExistsError(f"{conf_tools_ini} does not exist.")

    # Images folder
    if os.path.exists('images/'):
        print(f"images/ \t\t\t\t\t\t [OK]")
    else:
        raise FileExistsError(f"images/ does not exist.")

    print('.' * 100)

    print(f"Updating function name to: {gcf_name}")

    replace_string("blockchain_certificates_gcs", gcf_name, 'main.py')
    replace_string("blockchain_certificates_gcs", gcf_name, 'test_main.py')

    print("=" * 100)

    subprocess.run(
        "gcloud auth activate-service-account --key-file serviceAccountKey.json", shell=True, check=True)
    subprocess.run(
        f"gcloud config set project {project_id}", shell=True, check=True)
    subprocess.run("gcloud config get-value project", shell=True)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise ReferenceError("Missing argument <project>")
    project = sys.argv[1]
    configure_gcloud(project)
