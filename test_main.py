import os
import uuid
from unittest import mock
from unittest.mock import patch, mock_open

from dotenv import load_dotenv

import main

load_dotenv()

DIR_CLONE_REPOS = "/tmp/blockcerts"
STORAGE_INPUT_DIR = os.getenv("STORAGE_INPUT_DIR")


class TestGCFCertifyExample(object):
    @patch('main.storage_client')
    @patch('main.firestore_client')
    @patch('configure_certs.configure_blockcerts')
    @patch('certifier.certify_data')
    @patch("builtins.open", new_callable=mock_open, read_data="data".encode())
    @patch("os.listdir")
    def test_blockchain_certificates_gcs(
            self,
            mock_listdir,
            mock_file,
            certify_data,
            configure_blockcerts,
            firestore_client,
            storage_client,
            capsys):
        mock_listdir.return_value = ['demo.json']
        filename = f"TEST_DIR/{str(uuid.uuid4())}"
        project_id = 'neuquen-certificates'
        data = {
            "bucket": f"{project_id}.appspot.com",
            "name": f"{STORAGE_INPUT_DIR}/BFA_UPSO_2020_10_08_10_54_04.csv",
            "metageneration": "some-metageneration",
            "timeCreated": "0",
            "updated": "0"
        }
        print(data)
        bucket = storage_client.bucket.return_value
        file = bucket.file.return_value
        file.save.return_value = None
        certify_data.return_value = '/tmp/blockcerts/cert-issuer2'
        context = mock.MagicMock()
        context.event_id = 'some-id'
        context.event_type = 'gcs-event'

        main.blockchain_certificates_gcs(data, context)
        certify_data.assert_called()
        out, err = capsys.readouterr()
