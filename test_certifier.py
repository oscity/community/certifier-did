import os
from os.path import join
from unittest.mock import MagicMock, patch

from dotenv import load_dotenv

import certifier

load_dotenv()


class TestCertifier(object):

    def __instantiate_certificates_batch_mock(self, dir_name):
        """
        Generate testing instance unsigned certificates inside cert-tools directory
        Args:
            dir_name: Name of the instance, used as subdirecory of cert-tools

        Returns: os.system return call for a touch command
        """
        unsigned_certificates = join(
            '/tmp/blockcerts/cert-tools', dir_name, 'unsigned_certificates')
        print(unsigned_certificates)
        os.makedirs(unsigned_certificates, exist_ok=True)
        return os.system(f"touch {unsigned_certificates}/unsigned.json")

    def __issue_certificates_mock(self):
        """
        Generates demo JSON certificate file in cert-issuer blockchain certificates result
        Returns: os.system return call for a touch command
        """
        blockchain_certificates = join(
            '/tmp/blockcerts/cert-issuer2', 'data', 'blockchain_certificates')
        print(blockchain_certificates)
        os.makedirs(blockchain_certificates, exist_ok=True)
        return os.system(f"touch {blockchain_certificates}/certificate.json")

    @patch('certifier.__instantiate_certificates_batch')
    @patch('certifier.__issue_certificates')
    @patch('certifier.move_files')
    @patch('certifier.os')
    def test_certify_data(self, os_mock, __move_files, __instantiate_certificates_batch, __issue_certificates, capsys):
        os_mock.system = MagicMock(return_value=0)
        __instantiate_certificates_batch = MagicMock(return_value=0)
        __issue_certificates = MagicMock(return_value=0)
        dirname = 'rionegro'

        # Generate demo (empty) unsigned and blockchain certificates
        self.__instantiate_certificates_batch_mock(dirname)
        self.__issue_certificates_mock()

        certifier.certify_data(dirname)

        out, err = capsys.readouterr()
        print(out)

        assert __move_files.called
