#!/bin/python

# You can clean the current workspace, if gcloud.config file is specified, GCF_NAME variable
# will be used to restore the name of the function to its original name.
# All configurations files will be deleted!
# python3 run_clean_workspace.py

import configparser
import os
import subprocess


def clean_workspace():
    GCF_NAME = None
    config = configparser.ConfigParser()

    if os.path.exists('gcloud.config'):
        config.read('gcloud.config', 'utf-8')
        GCF_NAME = config['DEFAULT']['GCF_NAME']

    if not GCF_NAME:
        # Var is not specified, checkout changes in function
        print("Warning: GCF_NAME is not specified, all changes in main.py, and test_main.py will be checkout")
        subprocess.run(['git', 'checkout', 'main.py'])
        subprocess.run(['git', 'checkout', 'test_main.py'])
    else:
        # Replace function name with original one
        print("Restoring function name to original...")
        replace_string(GCF_NAME, 'blockchain_certificates_gcs', 'main.py')
        replace_string(GCF_NAME, 'blockchain_certificates_gcs', 'test_main.py')

    # Delete configuration files
    print("Deleting all configuration files...")
    subprocess.run('rm -f pk_**.txt', shell=True)
    subprocess.run('rm -f conf_**.ini', shell=True)
    subprocess.run('rm -rf images/', shell=True)
    subprocess.run('rm -f serviceAccountKey.json', shell=True)
    subprocess.run('rm -f gcloud.config', shell=True)
    subprocess.run('rm -f .env.yaml', shell=True)
    subprocess.run('rm -f .env', shell=True)

    print('Process finished.')


def replace_string(old_string, new_string, filename):
    file_in = open(filename, 'rt')
    data = file_in.read()
    data = data.replace(old_string, new_string)
    file_in.close()
    flie_out = open(filename, 'wt')
    flie_out.write(data)
    flie_out.close()


if __name__ == "__main__":
    clean_workspace()
