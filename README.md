<img src="https://avatars2.githubusercontent.com/u/2810941?v=3&s=96" alt="Google Cloud Platform logo" title="Google Cloud Platform" align="right" height="96" width="96"/>

# Google Cloud Functions - Create blockchain certificates using Cloud Storage and Firestore Database

This project includes a Google Cloud Function implementation to create Blockchain Certificates using Blockcerts Technology adapted by OS City.

It depends of [`cert-core`](https://gitlab.com/oscity/singletally/cert-core), [`cert-tools`](https://gitlab.com/oscity/singletally/cert-tools) and [`cert-issuer`](https://gitlab.com/oscity/singletally/cert-issuer2) implementetations found in OS City – Singletally related GitLab repositories that were included in [`requirements.txt`](requirements.txt)

This function uses a Cloud Storage trigger event to run, process a file, upload the result to the same Storage, and update a document in a Firestore collection.

1. File is uploaded to Cloud Storage and triggers the function.
2. Inside GCF, file is temporarily downloaded to process it as a roster for Blockchain certificates
3. Using async calls, each certificate in JSON format is uploaded to the same Storage (different directory to avoid re-triggering the function)
4. Using JSON data, the matching document in Firestore is updated with Blockchain certificate url.

## Development

### 1. Previous configuration

To use this Cloud Function, make sure that the [Cloud Functions and Cloud Storage APIs](https://console.cloud.google.com/flows/enableapi?apiid=cloudfunctions,storage_api&redirect=https://cloud.google.com/functions/docs/tutorials/storage&_ga=2.223290844.692415190.1591024730-472056349.1586972522) are enabled for your project, as well as Billing (Google has some limitations in time and external internet conection)

- [Google Cloud SDK must be installed and configured](https://cloud.google.com/sdk/docs)
- A service account key file must be generated in your project's [Service Accounts](https://console.cloud.google.com/iam-admin/serviceaccounts) and has to have access (full read and write operations) to Cloud Storage and Firestore, this file must be placed in source code directory.

  >  **Note:** This repo is configured to ignore `serviceAccountKey_*.json` files. You shouldn't upload this file to GIT.
- You'll also need a Private Key Issuer file that matches your chosen Blockchain and contains your private key. (Eg. `REF_PK_ISSUER_BFA.ini`)

  >  **Note:** This repo is configured to ignore `REF_PK_ISSUER_*.txt` files. You shouldn't upload this file to GIT.

### 2. Install requirements

To run the project locally is recommended to use Python version `3.7` with virtualenv.

> **Tip:** Check [pyenv](https://github.com/pyenv/pyenv) to easily switch between multiple versions of Python

```bash
# Create and load a virtualenv
virtualenv venv
source venv/bin/activate
# Install dependencies using pip
pip install -r requirements.txt
```

### 3. Configure the Cloud Function instance

Each client/instance has a different configuration based on Blockcerts configuration, domain and even the selected Blockchain net. 

All existent configurations are placed in the `config/` directory. You can configure an already defined project for development using the defined script `run_configure_gcloud.py`. You only need to specify the name of he directory as the first argument of the call.

```bash
python run_configure_gcloud.py <DIRNAME>
```

This will copy and adapt the files from the `config/<DIRNAME>` location to the working directory and validate each needed file exists. 

> As an example, the following code block is the result printed of the call `python run_configure_gcloud.py oscity_demo`

```
===========================================================
Configuring project: oscity_demo
===========================================================
gcloud configuration:
...........................................................
Google Cloud Project: os-city-platform-certificates
Google Cloud Function Name: blockchain_certificates_oscity_digital_id
===========================================================
Checking files exist:
.env.yaml                             [OK]
serviceAccountKey.json                [OK]
pk_issuer_eth.txt                     [OK]
conf_oscity_eth.ini                   [OK]
images/                               [OK]
...........................................................
Updating function name to: blockchain_certificates_oscity_digital_id
===========================================================
```

### 4. Develop and Tests

Now you can make changes in the local source code. Tests can be runned using `pytest`

```shell
pytest
```

### 5. Clean workspace

You can clean the workspace by using the script `run_clean_workspace.py`. It will restore original function name and delete all configuration files.

```bash
python run_clean_workspace.py
```

## Deploy

### Deploy an already configured function

#### Auto-deploy with GitLab CI/CD

Already configured functions are ready to self deploy using a GitLab CI/CD Pipeline. Whatever changes were merged into `master` they will be reflected in the configured Cloud Functions.

You can check [.gitlab-ci.yml](.gitlab-ci.yml) file to see the list of deployments configured.

#### Deploy manually using automatic script

You can deploy a project found in `config/` folder, using the defined script `run_deploy_gcloud.py`.

This script includes the **configuration** and **cleaning** steps, you only need to define the selected directory as the first argument of the call.

```bash
python run_deploy_gcloud.py <DIRNAME>
```

## Configure a new function instance

As we can see, there's a defined structure for all the instances configured for this Cloud Function. They all share this structure for their configuration in its own :

- Previously placed `REF_PK_ISSUER_<BLOCKCHAIN>.ini` file for local development
- Previously placed `serviceAccountKey_<PROJECT>.json` file for local development
- It's own directory placed inside `config/` that contains: 
  - `images/` directory with logos and all other images referenced in conf-tools file
  - `conf_<project>.ini` Config tools file (Check Blockcerts configuration)
  - `gcloud.config` file specifying [GCP Project configuration](#gcloud-config-file)
  - `.env.yaml` [Cloud function environtment variables file](#the-dotenv-yaml-file)

### Gcloud config file

To set GCP Project configuration easily, this file includes all the needed information about the project and its environtment variables for CI/CD configuration.

| VARIABLE         | MEANING                                                      |
| ---------------- | ------------------------------------------------------------ |
| `PROJECT_ID`     | GCP Project name                                             |
| `GCF_NAME`       | Cloud Function name when deployed                            |
| `PK_ISSUER_FILE` | Name of the Environtment Variable that contains the PK Issuer file (Check [GitLab CI/CD variables & The dotenv Local file](#GitLab-CI/CD-variables-&-The-dotenv-Local-file)) |
| `ACCOUNT_KEY`    | Name of the Environtment Variable that contains the Service Account Key File (Check [GitLab CI/CD variables & The dotenv Local file](#GitLab-CI/CD-variables-&-The-dotenv-Local-file)) |

### The dotenv YAML file

Cloud Functions allows to configure using environtment variables in a `.env.yaml` file. The following ones are configurable acording to your project:

| Variable                         | Meaning                                                     |
| -------------------------------- | ----------------------------------------------------------- |
| `GOOGLE_APPLICATION_CREDENTIALS` | Service Account Key file (Set to `serviceAccountKey.json`)  |
| `STORAGE_INPUT_DIR`              | Directory where rosters (CSV) will be uploaded to certify   |
| `STORAGE_OUTPUT_DIR`             | Directory where JSON signed certificates will be uploaded   |
| `DATABASE_COLLECTION`            | Firestore collection that has the certificates data         |
| `UNIQUE_KEY`                     | Firestore collection unique id property to match document   |
| `DOMAIN_URL`                     | Domain where certificates will be visualized                |
| `CONF_TOOLS_INI`                 | Name of the `conf_tools.ini` file deployed                  |
| `PK_ISSUER`                      | Name of private key `pk_issuer.txt` file deployed           |
| `ISSUING_ADDRESS`                | Public issuing address                                      |
| `ISSUER_CHAIN`                   | Blockchain chain selected                                   |

Using the files and variables described, the same base code can be used in different projects.

### GitLab CI/CD variables & The dotenv Local file

GitLab's CI/CD Auto-deployments use Environtment Variables to have access to the GCP Project and its resources, and also a secret Private Key to sign certificates in the selected Blockchain.

To configure auto-deployment in GitLab CI/CD you must add a **File** Environment Variable with the following convention:

| Variable                    | Meaning                                                      |
| --------------------------- | ------------------------------------------------------------ |
| `GCP_SERVICE_KEY_<PROJECT>` | Service Account Key File for each GCP Project to access resources |
| `PK_ISSUER_<BLOCKCHAIN>`    | Private Key File of selected Blockchain                      |

Aditionally, to deploy manually and test locally these variables have to be placed placed in the [.env.local](.env.local) file simulating the variables in GitLab.

## Calling already deployed cloud functions from CLI

For more info check [Calling Cloud Functions Directly](https://cloud.google.com/functions/docs/calling/direct?authuser=2)

```bash
gcloud functions call YOUR_FUNCTION_NAME --data '{"name":"Keyboard Cat"}'
```

For more information about Google Cloud Functions see:

- [How-to Guides](https://cloud.google.com/functions/docs/how-to)
- [Cloud Functions Cloud Storage tutorial](https://cloud.google.com/functions/docs/tutorials/storage)
- [ImageMagick Tutorial](https://cloud.google.com/functions/docs/tutorials/imagemagick)
- [Source code](main.py)


