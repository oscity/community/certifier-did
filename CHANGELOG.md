# [1.24.0](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.23.0...v1.24.0) (2022-01-13)


### Features

* Agregaron campos de fecha final y horas del curso ([d25ad4a](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/d25ad4a453cb0f198d061c2817e535b3c6b8cc5d))

# [1.23.0](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.22.0...v1.23.0) (2021-11-22)


### Bug Fixes

* FirstName for email ([e03c487](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/e03c48737f20c0932f1be2530fe99d3231a9b769))
* Json requests ([fef0544](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/fef0544d50139cd3bc347a0961c2d8fb420bc50f))


### Features

* Config eProof ([6d2aac5](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/6d2aac508d79c985bd37be64dd41f4430394f2a5))

# [1.22.0](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.21.0...v1.22.0) (2021-11-09)


### Bug Fixes

* Url endpoints cmp ([4e2ec0c](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/4e2ec0c0a5c4f6892e23d3a3ae49a6a928becf50))


### Features

* Logs ([ee20568](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/ee2056861f6a4fc73f673b05f531de90b411a4f1))
* Send email with certificate link ([217f424](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/217f4241e4663ccb8ea0daacba0f377dad039c81))

# [1.21.0](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.20.0...v1.21.0) (2021-11-01)


### Features

* Add type cert ([9969bef](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/9969bef5143a6bb452df20de25e7899f90662103))

# [1.20.0](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.19.0...v1.20.0) (2021-10-25)


### Bug Fixes

* Deploy ordenamiento territorial ([2b4bb92](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/2b4bb92e884e0957348e7016ee96360d7cc09760))


### Features

* Add deploy ordenamiento ([ec04b57](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/ec04b57b36935d6072d1c003dd6ac6c03d3bbb51))
* Domains Fondo and ordenamiento ([9181f15](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/9181f15e86a2d5341add205a0fcad5880bb3f55e))

# [1.19.0](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.18.0...v1.19.0) (2021-10-18)


### Features

* Config unicef ([dd79b8f](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/dd79b8fe654ef5db8fab667b2fba853047c922b0))

# [1.18.0](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.17.1...v1.18.0) (2021-10-18)


### Features

* Domain unicef - oscity ([5e0e949](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/5e0e949aedfea02ea4b5e169dc5b16596bcb7126))
* Unicef domain ([2b45228](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/2b4522874d4075babe75eb98bada3c2991f9c450))

## [1.17.1](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.17.0...v1.17.1) (2021-09-01)


### Bug Fixes

* Complete field minibingp ([3bd9780](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/3bd978092052ab24c74e62ef68c81806d069ec81))

# [1.17.0](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.16.0...v1.17.0) (2021-09-01)


### Features

* Add special award to minibingo ([f67974b](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/f67974bf5e23016b47e2043df5278409979e6435))

# [1.16.0](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.15.0...v1.16.0) (2021-08-18)


### Features

* Unicef key ([2592ec0](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/2592ec09c854e5b3da70ce0e6d11c3b0f0bd352e))

# [1.15.0](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.14.1...v1.15.0) (2021-07-05)


### Features

* Nuevos campos en carga de datos ([a2f549f](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/a2f549f21748a9336971c507793b3f3393f804c4))

## [1.14.1](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.14.0...v1.14.1) (2021-06-10)


### Bug Fixes

* Cloud Certif domain ([0cc61e1](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/0cc61e16907c7962d002a1bd7e0e5b63e881b757))

# [1.14.0](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.13.1...v1.14.0) (2021-06-09)


### Features

* Modify name to Config and gitlab ci corrected ([1ce6298](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/1ce62985281ef47e3b935b56a5d8b7145cab8dec))
* Setup Dervinsa Acid Certifier ([37cf7eb](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/37cf7eb5fa5fa468bd8327e43d9f72c1b3c92871))

## [1.13.1](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.13.0...v1.13.1) (2021-05-26)


### Bug Fixes

* Cambio de nombre para campo de certificado ([a6f0786](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/a6f0786b9ea96fb0b97b00babbcb7dd3ae5640fa))

# [1.13.0](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.12.2...v1.13.0) (2021-05-25)


### Bug Fixes

* Campos certificados ([0a9f476](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/0a9f476ab7526b9fc9301aa593955ffe256b9288))
* Remove DS_Store ([4485738](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/4485738355908b65fbff3430d1b96fc86daa160e))


### Features

* Config env misiones ([654c204](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/654c2046a97a641090403bd2f40a739eb859688a))
* Configuración deploy para fondo de crédito misiones ([b08bfcb](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/b08bfcb186ed0922ca2507964e6fc3a8add15821))
* Configuración para fondo de crédito misiones ([9926422](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/99264225b5fcb7c9d8ee331d969663d2607b20b0))
* Dominio para certificados ([9f9615f](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/9f9615f9b170d78d84bfb5344ab16735f79202bc))
* env local ([8a37a85](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/8a37a85c0f0e4851b73fc12576e61b8e739d4c9b))
* Function para misiones ([8ba9d0d](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/8ba9d0d619f23bb412d254c9e8a896b8f77788ac))
* Merge master ([6f216da](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/6f216dac38aa17e3d967026b6fb861c7e0a59c3e))
* Merge master ([84f9076](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/84f90769b20d1946c6f17fb1ba1a3a2703fe5d24))

## [1.12.2](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.12.1...v1.12.2) (2021-04-13)


### Bug Fixes

* Lacchain verify balance ([33f36dd](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/33f36ddc829433996fbdeedc394e6269189fe4a4))

## [1.12.1](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.12.0...v1.12.1) (2021-04-13)


### Bug Fixes

* Lacchain Ropstenm ([2da03f2](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/2da03f2e1d27711828ed69c3501554112dda4a3e))

# [1.12.0](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.11.0...v1.12.0) (2021-04-13)


### Features

* Commit para issuer ([ba86fa5](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/ba86fa5c48e6382932ccb1db10a2aed7f3071c94))

# [1.11.0](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.10.1...v1.11.0) (2021-04-13)


### Features

* Configuration for lacChain ([1199c56](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/1199c56fe5a00e1bc37477e498c35c8a321acdb2))

## [1.10.1](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.10.0...v1.10.1) (2021-03-18)


### Bug Fixes

* Created Gov Hub ([889beeb](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/889beeb86f3518c0cab0dbe2a79bb255b800e2da))

# [1.10.0](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.9.0...v1.10.0) (2021-03-18)


### Features

* Nuevos campos para minibingo ([1acfe32](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/1acfe32acad23154c74d85e960ff17d01865e7c1))

# [1.9.0](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.8.0...v1.9.0) (2021-03-18)


### Features

* GasPrice Up to 200 Gwei ([14e78a6](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/14e78a642509731cbc2a9213af6698cd65ca5745))

# [1.8.0](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.7.0...v1.8.0) (2021-03-18)


### Features

* GasPrice Up to 300 Gwei ([08851ce](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/08851ce640de21015373e609653ff65ed2447dd1))

# [1.7.0](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.6.1...v1.7.0) (2021-03-18)


### Features

* GasPrice Up ([3f5dbc4](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/3f5dbc4bacb71a655a9c5159e27c12bc9493da63))

## [1.6.1](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.6.0...v1.6.1) (2021-03-17)


### Bug Fixes

* Headewrs Latestst Commit on issuer2 ([7330615](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/7330615f77caf6bf415d9fea50e50fef395da1cc))

# [1.6.0](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.5.2...v1.6.0) (2021-02-11)


### Bug Fixes

* Error datatype for env variables in gcloud ([0ff4eb8](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/0ff4eb85e5458ddd88de928d53cce53dc59b388f)), closes [#24](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/issues/24)
* Set defacult gas price and custom value for ETH ([8c4625d](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/8c4625d9e02484f8f26832347d11a6ac03fb35b1))


### Features

* New env variable for gas price ([d125876](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/d1258769d6ffc692598d971f8b2cd4fce0dbadff))
* New field for artist signature ([4eed061](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/4eed061bed9b9694cea78bbd0513b651e4f4eaa6))
* New field for artist signature ([63b7566](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/63b75660a05bb18ecd00adf2b0a533171a829d96))
* Remove  env variable for gas price ([bce6b57](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/bce6b5755b45a352a564a6c233cd53565de6f076))

## [1.5.2](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.5.1...v1.5.2) (2021-01-07)


### Bug Fixes

* Add api_key parameter to config **only** if it exists ([58f3215](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/58f321588f72e17a41651ff8f90a773c2634aa80))

## [1.5.1](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.5.0...v1.5.1) (2021-01-06)


### Bug Fixes

* Add API KEY for ETH conf issuer ([5a65b3c](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/5a65b3cd12e7cac0532d409785f88044cff3b15b))

# [1.5.0](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.4.1...v1.5.0) (2021-01-04)


### Bug Fixes

* Corrección de nombre de archivo config ([2c1a30d](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/2c1a30d3f28e1e1f117886a7a6dfb4b54f7e1500))
* Corrección de nombre para carpeta data ([68b3805](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/68b38056a5eeb2162a1784d8e348c3bb462c8d4a))
* Corrección extensión logo ([45d3b4b](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/45d3b4b5a107346caa987f397c6f7fc4d539d1a5))


### Features

* Configuración deploy minibingo ([2003edc](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/2003edc22339e510a95959d772c81b5a8e23a967))
* Configuración para certificados minibingo ([bf87d0d](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/bf87d0d9bd778283aa1980a24a795f0223c99b1e))
* Logo minibingo ([f7a7441](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/f7a744177a780270c39c617f144ef3104c6ebccb))

## [1.4.1](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.4.0...v1.4.1) (2020-12-09)


### Bug Fixes

* **govtechhub:** Signature image extension ([5911f25](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/5911f257434ff4060afa78c30faf1d1b1f07d207))

# [1.4.0](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.3.2...v1.4.0) (2020-12-08)


### Features

* Dejando env igual que las demás instancias ([5df05de](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/5df05ded74a2a85f1953fc45f51d7ba7c0b6241a))

## [1.3.2](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.3.1...v1.3.2) (2020-12-08)


### Bug Fixes

* Cambio de id schema ([63a5e18](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/63a5e186560aefff60608d2e7ecfd91b6e3f21f7))

## [1.3.1](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.3.0...v1.3.1) (2020-12-07)


### Bug Fixes

* Config Abastible Added new field ([9c978ac](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/9c978ac9363392f21299f0811b1bf1d26daf4f46))

# [1.3.0](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.2.1...v1.3.0) (2020-12-04)


### Features

* Abastible Setup 4 Certificates ([0310fd4](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/0310fd44106acf108c1d26620805b1c5ad159a32))

## [1.2.1](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.2.0...v1.2.1) (2020-12-01)


### Bug Fixes

* Cambio de wallet a RSK ([cd99a02](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/cd99a023fbbe36a9317404306bb2e73126f22739))

# [1.2.0](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.1.0...v1.2.0) (2020-11-30)


### Bug Fixes

* Cambio nombre data_dir ([0687ac6](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/0687ac63bb0b955d1e9b554e13e3a2ef413f74c5))


### Features

* Agregó id_reference ([f885561](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/f885561d49f844f19d598b28ff2248c53477a81a))
* Configuración deploy - Ruth Benzacar ([096fc90](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/096fc90ea1a5778fbb152a05379dbea7d8f07843))
* Configuración para function - Ruth Benzacar ([661a770](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/661a770a4b2c2ed3f5d7d4a27e6bf1820ee12021))
* Dominio demos ([0121314](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/0121314b6add3b896fb65864cade837e7f88be1a))
* PK para ETH main ([ad64443](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/ad6444345b8c762fc88d07664a26da29d4796c0b))

# [1.1.0](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/compare/v1.0.0...v1.1.0) (2020-11-10)


### Bug Fixes

* [UPSO] Add header for config file gcloud ([752f286](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/752f2866cf44964408b1a6fef93a54ca032af8b1))
* Add default header for conf ini files (gcloud.conf) ([ad5177b](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/ad5177b49ed7792bc6d3e5aa82d2cb3e1065aa82))
* json in one line in config file ([ddb900e](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/ddb900ecdfdfb220fceea63556318dc7a1ddea3c))
* Remove prints of non existent properties ([70aae03](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/70aae0391946e6955946c7ae2d3d2475c1df8381))
* Remove unused code ([a2fedae](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/a2fedaeabc2c63f93ff525f0a7cba6ae519c9cc8))
* Sort requirements add pysha3 dependency ([235fe78](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/235fe7802be37bd7bfc69370672aba83ee14d868))
* Update certifier context url ([0533aaf](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/0533aaf824eae9aeaac22f8ed4c0ee0248bb8204))
* Update data dir for rionegro unificada ([c7c7069](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/c7c7069f352b9a7075565802c62e7edeca1f92ce))
* Update quepos configuration for licores ([90efbeb](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/90efbeb09889bca0f886fb3ad26cce6c58f652de)), closes [#8](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/issues/8)
* Update rionegro unificada conf tools ([661e34d](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/661e34d355ed40b11725a90522ec6c02611558d2))
* Validate year is actually a year before updating date format ([d481f6e](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/d481f6e565406eb709adfd1fe675b17a838992ce))
* **upso:** Update roster name ([693eb3c](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/693eb3cecc55f337049f7c31d455e6daed267e49))
* Use tmp uuid as filename before cleaning roster ([44a3ddf](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/44a3ddff0f9753ef66933c8d065eecaed116a370))


### Features

* Add quepos licores configuration ([f05b54d](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/f05b54d917cc60ef3231436f473cf46993d3906d))
* Add scripts to configure, deploy in gcloud and clean project ([983a6f5](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/983a6f5f6f7677caf8a83bbb4433eae8354f375c))
* Clean roaster file. Get rid of Windows characters non utf-8. ([9c29599](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/9c29599d3f8253e1b0ad03f53acc37dbd85a88f3))
* Conf Quepos Licor latest ([60d20cf](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/60d20cfe83a69df34d4631b91f4aa9a396bb7df6))
* Conf Quepos Patentes latest ([ac125f8](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/ac125f82f81ace1d1e406cdfc0113e548086ef11))
* Conf Quepos Patentes latest Gitlab Readable ([3d6217f](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/3d6217f86bc761a5d7023c5b9781ef0bf220ce90))
* Config Quepos Licores Ok ([24054a3](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/24054a36665d84dbe59391a59470969960142117))
* config Quepos Licores y PAtentes ([fe9e8b1](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/fe9e8b1dda09b5545dd0ed7fda3b787622319bd1))
* Config Quepos Patentes Ok ([9eae551](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/9eae5511c00ba18ec1d35628a54170d566507946))
* Test image logo file exist ([e16c777](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/e16c77732914a3fc2d9d8a833f5e91dbaaab0e49))
* Update script with asyncio ([e276a4d](https://gitlab.com/oscity/singletally/google-cloud-function-certifier/commit/e276a4dd9f885eb826dac70e15a5f28d7c6acd8b))
