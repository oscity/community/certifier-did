import asyncio
import configparser
import datetime
import json
import os

import requests
from dotenv import load_dotenv
from google.cloud import storage, firestore
from loguru import logger

load_dotenv()

CONF_TOOLS_INI = os.getenv("CONF_TOOLS_INI")
DATABASE_COLLECTION = os.getenv("DATABASE_COLLECTION")
DATA_DIR = os.getenv("DATA_DIR")
DOMAIN_URL = os.getenv("DOMAIN_URL")
STORAGE_OUTPUT_DIR = os.getenv("STORAGE_OUTPUT_DIR")
UNIQUE_KEY = os.getenv("UNIQUE_KEY")

now = datetime.datetime.now()

firebase_client = firestore.Client()
storage_client = storage.Client()

config = configparser.ConfigParser()
config.read('gcloud.config')
GCF_NAME = config['DEFAULT']['GCF_NAME']
PROJECT_ID = config['DEFAULT']['PROJECT_ID']

revocation = json.loads(requests.get(
    f"{DOMAIN_URL}issuer/revocation.json").text)
# TODO: Get revocation file name from certificates.
# Eg. UPSO is not normalized and uses revocation-upso.json file
# Option: Use ["badge"]["revocationList"]


async def create_documents():
    """
    List all blobs in specified Storage directory and verify if they exist in database
    Returns:

    """
    document = firebase_client.collection(
        'upso').document('rib6ktHsHOlCIkgqvW7A')
    json_data = document.get().to_dict()
    logger.info(json_data)

    new_document = firebase_client.collection(
        'upso-dev').document('rib6ktHsHOlCIkgqvW7A')
    # new_document.set(json_data)


async def homologate_dates_to_iso():
    """
    Get all documents and change its creation date to iso 8601
    """
    query = __set_query_unique_match("Certificado")
    docs = firebase_client.collection(
        DATABASE_COLLECTION).where(*query).stream()
    for doc in docs:
        await __update_document_date(doc)


async def __update_document_date(doc):
    document = firebase_client.collection(
        DATABASE_COLLECTION).document(doc.id)
    data = doc.to_dict()
    current_date = data["created"]
    logger.info('current  ' + current_date)
    day, month, year = current_date.split('-')
    if (int(year) > 1900):
        iso_date = f'{year}-{month}-{day}'
        logger.info('iso date ' + iso_date)
        result = document.update(
            {"created": iso_date})
        logger.info(result)


async def reset_no_certified_data():
    """
    Get all documents and change its creation date to iso 8601
    """
    query = __set_query_unique_match("En espera")
    docs = firebase_client.collection(
        DATABASE_COLLECTION).where(*query).stream()
    for doc in docs:
        await __update_status_no_certified(doc)


async def __update_status_no_certified(doc):
    document = firebase_client.collection(
        DATABASE_COLLECTION).document(doc.id)
    result = document.update(
        {"estado_origen": "Sin certificar", "certify": False})
    logger.info(result)


def __set_query_unique_match(estado_origen):
    """
    Creates Firebase query.
    Args:
        estado_origen:

    Returns:

    """
    return 'estado_origen', "==", estado_origen


if __name__ == "__main__":
    asyncio.run(reset_no_certified_data())
