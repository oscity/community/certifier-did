import asyncio
import configparser
import datetime
import json
import os

import requests
from dotenv import load_dotenv
from google.cloud import storage, firestore
from loguru import logger

load_dotenv()

CONF_TOOLS_INI = os.getenv("CONF_TOOLS_INI")
DATABASE_COLLECTION = os.getenv("DATABASE_COLLECTION")
DATA_DIR = os.getenv("DATA_DIR")
DOMAIN_URL = os.getenv("DOMAIN_URL")
STORAGE_OUTPUT_DIR = os.getenv("STORAGE_OUTPUT_DIR")
UNIQUE_KEY = os.getenv("UNIQUE_KEY")

now = datetime.datetime.now()
logger.add(f"log-{DATABASE_COLLECTION}-{now}.log", enqueue=True)

firebase_client = firestore.Client()
storage_client = storage.Client()

config = configparser.ConfigParser()
config.read('gcloud.config')
GCF_NAME = config['DEFAULT']['GCF_NAME']
PROJECT_ID = config['DEFAULT']['PROJECT_ID']

revocation = json.loads(requests.get(
    f"{DOMAIN_URL}issuer/revocation.json").text)
# TODO: Get revocation file name from certificates.
# Eg. UPSO is not normalized and uses revocation-upso.json file
# Option: Use ["badge"]["revocationList"]


async def create_documents_from_files_in_storage():
    """
    List all blobs in specified Storage directory and verify if they exist in database
    Returns:

    """
    blobs = storage_client.list_blobs(
        PROJECT_ID + '.appspot.com', prefix=STORAGE_OUTPUT_DIR)
    for blob in blobs:
        await _look_up_blob(blob)


async def _look_up_blob(blob):
    """
    Validate if a document in database exists with blob information
    Check and update if certificate was revoked
    Create a new document if there wasn't a match in db
    Args:
        blob: Cloud Storage blob

    Returns:

    """
    # Get cert id from filename
    cert_id = blob.name.split('/').pop().replace('.json', '')
    # Generate URL to match using domain
    link = f"{DOMAIN_URL}{cert_id}"
    logger.info(link)

    # Set query for unique match
    query = __set_query_unique_match(link)
    # Get documents that match
    docs = firebase_client.collection(
        DATABASE_COLLECTION).where(*query).stream()
    for doc in docs:
        # Update status if cert were revoked
        revoked = [cert for cert in revocation['revokedAssertions']
                   if cert['id'].replace('urn:uuid:', '') == cert_id]
        if len(revoked) > 0 and doc.to_dict()['estado_origen'] != "Revocado":
            # Cert was revoked, update document
            logger.info('Cert was revoked!')
            document = firebase_client.collection(
                DATABASE_COLLECTION).document(doc.id)
            result = document.update(
                {"estado_origen": "Revocado", "is_revocated": True})
            logger.info(result)
        # A document with the link reference already exists. Exit call.
        return
    # A document matching was not found, create one from blob
    await _process_blob(blob)


async def _process_blob(blob):
    """
    Generate document to write in database
    Args:
        blob: Cloud Storage blob

    Returns:

    """
    json_data = blob.download_as_string()
    json_data = json.loads(json_data)

    # Mad patch  <-  Rio Negro Unificada & Certificates share Storage directory
    if DATABASE_COLLECTION == 'unificadaRioNegro':
        if 'nombre' in json_data:
            # Is not Unificada
            return
        json_data['nombre'] = 'Unificada'
    if DATABASE_COLLECTION == 'certificateDataCollection':
        if 'nombre' not in json_data:
            # Is Unificada
            return

    # Document properties to update
    json_data['created'] = datetime.datetime.fromisoformat(
        json_data['issuedOn']).strftime("%d-%m-%Y")
    json_data['cert_id'] = json_data['id'].replace('urn:uuid:', '')
    json_data["link"] = f"{DOMAIN_URL}{json_data['cert_id']}"
    json_data["certify"] = True
    json_data["is_certified"] = True

    # Check if cert was revoked
    revoked = [cert for cert in revocation['revokedAssertions']
               if cert['id'].replace('urn:uuid:', '') == json_data['cert_id']]
    if len(revoked) > 0:
        # Cert was revoked
        json_data["estado_origen"] = "Revocado"
        json_data["is_revocated"] = True
    else:
        json_data["estado_origen"] = "Certificado"

    recipient = json_data['recipient']
    verification = json_data['verification']
    del json_data["badge"]
    del json_data['@context']
    del json_data['recipient']
    del json_data['recipientProfile']
    del json_data['signature']
    del json_data['verification']
    json_data['pubkey'] = verification['publicKey']
    json_data['identity'] = recipient['identity']

    await __update_document(json_data)


async def __update_document(json_data):
    """
    Update Firebase document based in json_data
    Args:
        json_data: Data to update Firebase document

    Returns:

    """
    # New firebase document reference
    document_reference = firebase_client.collection(
        DATABASE_COLLECTION).document()

    logger.info(document_reference.id)
    json_data["id_reference"] = document_reference.id
    logger.info(json_data)
    # Update matching document with info from json
    result = document_reference.set(json_data)
    logger.info(result)
    return


def __set_query_unique_match(link):
    """
    Creates Firebase query.
    Args:
        link:

    Returns:

    """
    return 'link', "==", link


if __name__ == "__main__":
    asyncio.run(create_documents_from_files_in_storage())
